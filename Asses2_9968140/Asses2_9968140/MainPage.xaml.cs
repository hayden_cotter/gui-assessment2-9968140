﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Asses2_9968140
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            //Creating list
            var comboBoxToSource = new List<string>();
            comboBoxToSource.Add("Sales");
            comboBoxToSource.Add("IT");
            comboBoxToSource.Add("HR");
            var comboBoxSubjectSource = new List<string>();
            comboBoxSubjectSource.Add("Issue");
            comboBoxSubjectSource.Add("Meeting");
            comboBoxSubjectSource.Add("Casual");

            //Filling comboBoxes with items from list
            comboBoxTo.ItemsSource = comboBoxToSource;
            comboBoxSubject.ItemsSource = comboBoxSubjectSource;
        }

        private async void buttonSubmit_Click(object sender, RoutedEventArgs e)
        {
            //Creating a dialog with the from, subject and message strings
            var dialog = new MessageDialog($"From: {textBoxFrom.Text} \nSubject: {comboBoxSubject.SelectedValue} \nMessage: {textBoxBody.Text}");

            //Adding buttons to dialog
            dialog.Commands.Add(new UICommand { Label = "Send", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            dialog.Commands.Add(new UICommand { Label = "Reset", Id = 2 });

            //Opening dialog and waiting for response
            var res = await dialog.ShowAsync();

            //If the Reset button is clicked clear all fields
            if ((int)res.Id == 2)
            {
                comboBoxTo.SelectedIndex = -1;
                textBoxFrom.Text = "";

                comboBoxSubject.SelectedIndex = -1;
                textBoxBody.Text = "";
            }
        }
    }
}
